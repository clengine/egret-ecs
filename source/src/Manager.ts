module ecs{
    /**
     * 一个管理世界实体的管理器。
     */
    export abstract class Manager extends BaseSystem{
        /** 注册管理器 */
        public registerManager(){
            this._world.getAspectSubscriptionManager();
            // TODO add listener
        }
    }
}