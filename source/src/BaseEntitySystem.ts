/// <reference path="./BaseSystem.ts" />
module ecs {
    /**
     * 跟踪实体的子集，但不实现任何排序或迭代。
     */
    export abstract class BaseEntitySystem extends BaseSystem{

    }
}