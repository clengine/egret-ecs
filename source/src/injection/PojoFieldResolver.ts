module ecs {
    /**
     * 手动注册对象的字段解析器，用于按类型或名称注入。
     */
    export interface PojoFieldResolver extends FieldHandler {
        /**
         * 设置人为注册的对象
         * @param pojos 
         */
        setPojos(pojos: {[s: string]: Object});
    }
}