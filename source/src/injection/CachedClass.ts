module ecs{
    export class CachedClass {
        /**
         * 这个CachedClass表示的类
         */
        public clazz: any;

        constructor(clazz: any){
            this.clazz = clazz;
        }

        /**
         * 所有与类相关的字段
         */
        public allFields: Field[];

        public injectInherited: boolean;
        public failOnNull: boolean;
    }
}