module ecs {
    /**
     * 
     */
    export interface Injector{
        /**
         * 已注册对象的程序化检索。
         * @param id 
         */
        getRegistered<T>(id: string): T;
        /**
         * 
         * @param world 
         * @param injectables 
         */
        initialize(world: World, injectables: {[s: string]: Object});
        /**
         * 
         * @param target 
         */
        inject(target: Object);
        /**
         * 确定目标对象是否可以由这个注入器注入
         * @param target 
         */
        isInjectable(target: Object): boolean;
        /**
         * 允许使用自定义{@link injection}配置注入器。将用于解析目标字段的实例值。
         * @param fieldHandler 用于解析依赖项值的fieldHandler
         */
        setFieldHandler(fieldHandler: FieldHandler): Injector;
    }
}