module ecs {
    /**
     * Enum用于根据类在ecs中的使用来缓存类类型。
     */
    export enum ClassType{
        /**
         * 用于{@link ComponentMapper}的(子)类
         */
        MAPPER,
        /**
         * 用于{@link BaseSystem}的(子)类
         */
        SYSTEM,
        /**
         * 用于{@link World}的(子)类
         */
        WORLD,
        /**
         * 用于其他任何东西。
         */
        CUSTOM
    }
}