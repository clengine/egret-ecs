module ecs {
    /**
     * 用于{@link FieldHandler}解析符合注入条件的类中的字段值的API。
     */
    export interface FieldResolver{
        /**
         * 
         * @param world 
         */
        initialize(world: World);
        /**
         * 
         * @param target 
         * @param fieldType 
         * @param field 
         */
        resolve(target: Object, fieldType: any, field: Field);
    }
}