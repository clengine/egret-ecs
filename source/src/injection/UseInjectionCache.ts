module ecs {
    /**
     * 在调用{@link FieldResolver#initialize(World)}之前
     * 在{@link FieldResolver#initialize(World)}期间
     * 实现此接口将调用{@link #setCache(InjectionCache)}方法。
     */
    export interface UseInjectionCache{
        /**
         * 
         * @param cache 
         */
        setCache(cache: InjectionCache);
    }
}