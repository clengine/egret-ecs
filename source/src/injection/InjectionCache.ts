module ecs {
    export class InjectionCache {
        private _fieldClassTypeCache: Map<any, ClassType> = new Map([]);
        private _classCache: Map<any, CachedClass> = new Map([]);
        private static _instance: InjectionCache;

        public static get(): InjectionCache{
            if (!this._instance)
                this._instance = new InjectionCache();

            return this._instance;
        }

        public getFieldClassType(fieldType: any): ClassType{
            let injectionType = this._fieldClassTypeCache[fieldType] as ClassType;
            if (!injectionType){
                if (ClassReflection.isAssignableFrom(ComponentMapper, fieldType)){
                    injectionType = ClassType.MAPPER;
                } else if(ClassReflection.isAssignableFrom(BaseSystem, fieldType)){
                    injectionType = ClassType.SYSTEM;
                } else if(ClassReflection.isAssignableFrom(World, fieldType)){
                    injectionType = ClassType.WORLD;
                } else {
                    injectionType = ClassType.CUSTOM;
                }
                this._fieldClassTypeCache[fieldType] = injectionType;
            }

            return injectionType;
        }

        public getCachedClass(clazz: any): CachedClass{
            let cachedClass = this._classCache.get(clazz);
            if (cachedClass == null){
                cachedClass = new CachedClass(clazz);

                this._classCache.add(clazz, cachedClass);
            }

            return cachedClass;
        }
    }
}