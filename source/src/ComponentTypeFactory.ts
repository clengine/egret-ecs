module ecs{
    /**
     * 跟踪单个世界中的所有组件类型。
     */
    export class ComponentTypeFactory{
        /** 包含所有生成的组件类型，新生成的组件类型将存储在这里。 */
        private _componentTypes: Map<Component, ComponentType> = new Map<Component, ComponentType>([]);
        /** 组件类型中此组件类型的索引 */
        public types: Bag<ComponentType> = new Bag<ComponentType>();
        public initialMapperCapacity;
        private _cm: ComponentManager;

        constructor(cm: ComponentManager, entityContainerSize: number){
            this._cm = cm;
            this.initialMapperCapacity = entityContainerSize;
        }

        /**
         * 获取给定组件类的组件类型的索引。
         * @param c 
         */
        public getIndexFor(c: Component){
            return this.getTypeFor(c).getIndex();
        }

        /**
         * 按索引获取组件类型。
         * @param index 
         */
        public getTypeFor(c: Component): ComponentType{
            let type = this._componentTypes.get(c);
            if (type == null)
                type = this.createComponentType(c);

            return type;
        }

        private createComponentType(c: Component): ComponentType{
            let type = new ComponentType(c, this.types.size());
            this._componentTypes.add(c, type);
            this.types.add(type);

            this._cm.registerComponentType(type, this.initialMapperCapacity);

            // TODO: listener create

            return type;
        }
    }
}