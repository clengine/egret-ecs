module ecs {
    export class FluidEntityPlugin implements Plugin {
        setup(b: WorldConfigurationBuilder) {
            b.dependsOn(Priority.HIGH, new SuperMapper());
            b.register(new ESubscriptionAspectResolver());
        }

    }

    /**
     * 支持esub归属的解析器
     */
    export class ESubscriptionAspectResolver implements FieldResolver{
        private _aspectFieldResolver: AspectFieldResolver = new AspectFieldResolver();

        public initialize(world: World) {
            this._aspectFieldResolver.initialize(world);
        }

        public resolve(target: Object, fieldType: any, field: Field) {
            return null;
        }

    }
}