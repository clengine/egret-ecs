/// <reference path="./BaseComponentMapper.ts" />
module ecs {
    /**
     * 从系统内部提供高性能组件访问和更改。
     * 
     * 这是修改组合和访问组件的推荐方法。组件映射器与传送器一样快。
     */
    export class ComponentMapper<A extends Component> extends BaseComponentMapper<A> {
        public components: Bag<A>;
        private _pool: ComponentPool<PooledComponent>;
        private _purgatory: ComponentRemover<A>;

        constructor(type: A, world: World){
            super(world.getComponentManager().typeFactory.getTypeFor(type));
            this.components = new Bag<A>();

            //TODO: _POOL INITIALIZE
        }

        /**
         * 快速但不安全地检索此实体的组件。
         * @param entityId 
         */
        public get(entityId: number): A{
            return this.components.get(entityId);
        }

        public create(entityId: number): A {
            let component = this.get(entityId);
            if (component == null || this._purgatory.unmark(entityId)){
                //TODO: create new
            }

            return component;
        }
        public remove(entityId: number) {
            throw new Error("Method not implemented.");
        }
    }
}