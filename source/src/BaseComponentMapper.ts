module ecs {
    export abstract class BaseComponentMapper<A extends Component>{
        /** 此映射程序处理的组件的类型。 */
        public type: ComponentType;

        constructor(type: ComponentType){
            this.type = type;
        }

        public abstract create(entityId: number): A;

        public abstract remove(entityId: number);

        /**
         * 从实体中创建或删除组件。
         * @param entityId 
         * @param value 
         */
        public set(entityId: number, value: boolean): A{
            if (value){
                return this.create(entityId);
            }else{
                this.remove(entityId);
                return null;
            }
        }
    }
}