declare interface Number {
    rightMove(pos: number): number;
}

declare interface Array<T> {
    copy(srcPos: number, dest: Array<T>, destPos: number, length: number) ;
}

Array.prototype.copy = function(srcPos, dest, destPos, length){
    for (let i = srcPos, offset = 0; i < length; i ++, offset ++){
        this[i] = dest[destPos + offset];
    }
}

Number.prototype.rightMove = function(pos: number){
    let value = Number(this);
    if (pos != 0){
        let mask = Number.MAX_VALUE;
        value = value >> 1;
        value = value & mask;
        value = value >> pos - 1;
    }

    return value;
}
