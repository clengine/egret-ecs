module ecs {
    /**
     * 不可修改
     * 
     * 可以存放副本的装置。也称为多集
     */
    export interface ImmutableBag<E> {
        /**
         * 返回Bag中指定位置的元素。
         * @param index 要返回的元素的索引
         */
        get(index: number): E;
        /**
         * 返回包中的元素数量。
         */
        size(): number;
        /**
         * 如果此包不包含任何元素，则返回true
         */
        isEmpty(): boolean;
        /**
         * 检查bag是否包含此元素
         * @param e 
         */
        contains(e: E): boolean;
    }
}
