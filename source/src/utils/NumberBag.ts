module ecs {
    /**
     * 集合类型有点像Array，但不保留其实体的顺序，speedwise非常好，尤其适合于游戏
     */
    export class NumberBag implements ImmutableBag<number> {
        private _data: number[];
        private _size: number = 0;

        constructor(capacity: number = 64){
            this._data = new Array<number>(capacity);
        }

        public get(index: number): number {
            if (index >= this._size){
                let message = "tried accessing element " + index + "/" + this._size;
                throw new Error(message);
            }

            return this._data[index];
        }

        public size(): number {
            return this._size;
        }

        /**
         * 
         * @param size 
         */
        public setSize(size: number){
            this._size = size;
        }

        public getData(): number[]{
            return this._data;
        }

        public isEmpty(): boolean {
            return this._size == 0;
        }

        public ensureCapacity(index: number){
            if (index >= this._data.length)
                this.grow(index + 1);
        }

        private grow(newCapacity: number){
            let oldData = this._data;
            this._data = new Number[newCapacity];
            oldData.copy(0, this._data, 0, oldData.length);
        }

        public contains(value: number): boolean {
            for (let i = 0; this._size > i; i ++){
                if (value == this._data[i])
                    return true;
            }

            return false;
        }

    }
}