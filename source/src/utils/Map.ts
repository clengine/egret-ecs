module ecs {
    /** 词典类 */
    export class Map<T, U> {
        private _keys: T[] = [];
        private _values: U[] = [];

        constructor(init: { key: T; value: U; }[]) {
            for (let i = 0; i < init.length; i++) {
                this._keys.push(init[i].key);
                this._values.push(init[i].value);
            }
        }

        public get(key: T){
            for (let i = 0; i < this._keys.length; i ++){
                if (JSON.stringify(this._keys[i]) == JSON.stringify(key))
                    return this._values[i];
            }

            return null;
        }

        public add(key: T, value: U) {
            this._keys.push(key);
            this._values.push(value);
        }

        public remove(key: T) {
            let index = this._keys.indexOf(key, 0);
            this._keys.splice(index, 1);
            this._values.splice(index, 1);
        }

        public keys(): T[] {
            return this._keys;
        }

        public values(): U[] {
            return this._values;
        }

        public clear() {
            this._keys.splice(0);
            this._values.splice(0);
        }
    }
}
