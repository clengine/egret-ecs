module ecs {
    export class MathUtils {
        /**
         * 将值限制在指定范围内。
         * @param value 
         * @param min 
         * @param max 
         */
        public static clamp(value: number, min: number, max: number){
            value = (value > max) ? max : value;
            value = (value < min) ? min : value;
            return value;
        }
    }
}