/// <reference path="./SystemInvocationStrategy.ts" />
module ecs {
    /**
     * 简单顺序调用策略
     */
    export class InvocationStrategy extends SystemInvocationStrategy {
        /**
         * 按顺序处理所有系统
         * 
         * 应该确保在每次调用一个系统之前，在调用最后一个系统之后，或者根本没有调用任何系统时，使用对#updateEntityStates的调用使ecs处于正常状态。
         */
        public process() {
            let systemsData = this.systems.getData();
            for (let i = 0, s = this.systems.size(); s > i; i ++){
                if (this.disabled.get(i))
                    continue;

                this.updateEntityStates();
                systemsData[i].process();
            }

            this.updateEntityStates();
        }
    }
}