module ecs {
    /**
     * 类反射的实用程序
     */
    export class ClassReflection {
        /**
         * 确定由第一个类参数表示的类或接口是否与由第二个类参数表示的类或接口相同，或是否为该类或接口的超类或超接口
         * @param c1 
         * @param c2 
         */
        public static isAssignableFrom(c1: any, c2: any): boolean {
            return c2 instanceof c1;
        }
    }
}