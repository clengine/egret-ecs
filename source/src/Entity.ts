module ecs {
    /**
     * 实体类
     * 
     * 实体实例和id被回收。在实体从世界中删除后保留对该实体的引用是不安全的。
     * 
     * 不能在框架外实例化，必须使用World创建新实体。世界通过它的实体管理器创建实体。
     */
    export class Entity {
        /** 世界中的实体标识符 */
        public id: number;
        /** 这个实体所属的世界 */
        private _world: World;

        /**
         * 在给定的世界中创建一个新的{@link Entity}实例。
         * 这将只由世界通过它的实体管理器调用，而不是由用户直接调用，因为世界处理实体的创建。
         * @param world 创建实体的世界
         * @param id 要设置的id
         */
        constructor(world: World, id: number){
            this._world = world;
            this.id = id;
        }

        /** 
         * 框架内此实体的内部id。Id为0或更大。
         * 没有其他实体具有相同的ID，如果前一个实体被删除，另一个实体可能会获得此ID
         */
        public getId(): number{
            return this.id;
        }

        /**
         * 返回该实体所属的世界。
         */
        public getWorld(): World{
            return this._world;
        }
    }
}
