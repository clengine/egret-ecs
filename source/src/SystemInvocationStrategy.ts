module ecs {
    /**
     * 用于系统调用的委托。
     * 
     * 也许您希望对系统调用进行更细粒度的控制，为某些系统提供不同的增量，或者完全重写处理以支持事件。
     * 扩展这个类允许您为处理系统调用编写自己的逻辑。
     */
    export abstract class SystemInvocationStrategy {
        /** 世界 */
        public world: World;
        public systems: Bag<BaseSystem>;
        public disabled: BitVector = new BitVector();

        public abstract process();
        /** 在世界初始化阶段调用。 */
        public initialize(){}
        /**
         * 在{@link #initialize()之前调用
         * @param world 
         */
        public setWorld(world: World){
            this.world = world;
        }

        /**
         * 在{@link #initialize()之前调用
         * @param systems 
         */
        public setSystems(systems: Bag<BaseSystem>){
            this.systems = systems;
        }

        /**
         * 通知所有系统和订阅世界状态变化
         */
        public updateEntityStates(){
            this.world.batchProcessor.update();
        }
    }
}