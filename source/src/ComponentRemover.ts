module ecs{
    export abstract class ComponentRemover<A extends Component>{
        public pool: ComponentPool<PooledComponent>;
        private _components: Bag<A>;

        constructor(components: Bag<A>, pool: ComponentPool<PooledComponent>){
            this._components = components;
            this.pool = pool;
        }

        abstract mark(entity: number);
        abstract unmark(entity: number): boolean;
        abstract purge();
        abstract has(entityId: number): boolean;
    }
}