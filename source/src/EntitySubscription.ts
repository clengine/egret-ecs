module ecs {
    export class EntitySubscription{
        public extra: SubscriptionExtra;

        private _activeEntityIds: BitVector;
        private _entities: NumberBag;

        constructor(world: World, builder: Builder){
            this.extra = new SubscriptionExtra(builder.build(world), builder);

            this._activeEntityIds = new BitVector();
            this._entities = new NumberBag();

            let em = world.getEntityManager();
            em.registerEntityStore(this._activeEntityIds);
        }

        public getEntities(): NumberBag{
            if (this._entities.isEmpty() && !this._activeEntityIds.isEmpty()){
                this.rebuildCompressedActives();
            }

            return this._entities;
        }

        public rebuildCompressedActives(){
            this._activeEntityIds.toIntBag(this._entities);
        }
    }

    export interface SubscriptionListener{
        inserted(entities: NumberBag);
        removed(entities: NumberBag);
    }

    export class SubscriptionExtra {
        public aspect: Aspect;
        public aspectReflection: Builder;

        constructor(aspect: Aspect, aspectReflection: Builder){
            this.aspect = aspect;
            this.aspectReflection = aspectReflection;
        }
    }
}