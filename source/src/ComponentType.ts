module ecs {
    /**
     * 标识了不需要使用类中的组件。
     * 包含用于组件类型的序号，这允许快速检索组件。
     */
    export class ComponentType {
        private _type: Component;
        private _index: number;

        constructor(type: Component, index: number){
            this._index = index;
            this._type = type;
        }

        /**
         * 获取组件类型的索引。
         */
        public getIndex(): number{
            return this._index;
        }

        /**
         * 
         */
        public getType(): Component{
            return this._type;
        }
    }
}