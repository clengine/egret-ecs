module ecs {
    export abstract class LinkSite implements SubscriptionListener {
        public subscription: EntitySubscription;
        public field: Field;
        public type: ComponentType;
        public mapper: ComponentMapper<Component>;
        public activeEntityIds: BitVector;

        constructor(world: World, type: ComponentType, field: Field){
            this.type = type;
            this.field = field;

            this.mapper = world.getMapper(type.getType());
            // this.activeEntityIds = world.getAspectSubscriptionManager();
        }

        public inserted(entities: NumberBag) {
            let ids = entities.getData();
            for (let i = 0, s = entities.size(); s > i; i++) {
                this.insert(ids[i]);
            }
        }

        protected abstract insert(id: number);

        public removed(entities: NumberBag) {
            let ids = entities.getData();
            for (let i = 0, s = entities.size(); s > i; i++) {
                this.remove(ids[i]);
            }
        }

        protected abstract remove(id: number);

        public process(){
            let entities = this.subscription.getEntities();
            let ids = entities.getData();
            for (let i = 0, s = entities.size(); s > i; i ++)
                this.check(ids[i]);
        }

        protected abstract check(id: number);
    }
}