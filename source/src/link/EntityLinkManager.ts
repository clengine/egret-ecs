/// <reference path="../BaseEntitySystem.ts" />
module ecs {
    /**
     * 维护实体之间的关系
     * 
     * 此系统是可选的，必须手动向world实例注册
     */
    export class EntityLinkManager extends BaseEntitySystem {
        public linkSites: Bag<LinkSite> = new Bag<LinkSite>();
        public decoratedLinkSite: Bag<LinkSite> = new Bag<LinkSite>();

        private _requireListener: boolean;
        private _fireEventsOnRegistration;

        /**
         * 
         * @param processSitesEvenIfNoListener 如果为true，则仅对带有附加{@link LinkListener}的字段进行操作。
         * @param fireEventsOnRegistration 
         */
        constructor(processSitesEvenIfNoListener: boolean = true, fireEventsOnRegistration: boolean = true){
            super();
            this._requireListener = !processSitesEvenIfNoListener;
            this._fireEventsOnRegistration = fireEventsOnRegistration;
        }

        protected processSystem() {
            if (this._requireListener){
                this.processLink(this.decoratedLinkSite);
            }else{
                this.processLink(this.linkSites);
            }
        }

        private processLink(sites: Bag<LinkSite>){
            for (let i = 0; i < sites.size(); i ++){
                sites.get(i).process();
            }
        }
    }
}