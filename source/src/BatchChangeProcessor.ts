module ecs {
    export class BatchChangeProcessor {
        private _world: World;
        private _asm: AspectSubscriptionManager;

        public changed: BitVector = new BitVector();
        /** 标记为删除，将为实体订阅尽快删除 */
        private _deleted: BitVector = new BitVector();

        constructor(world: World){
            this._world = world;
            this._asm = world.getAspectSubscriptionManager();
        }

        public update(){
            while (!this.changed.isEmpty() || !this._deleted.isEmpty()){
                this._asm.processNotify(this.changed, this._deleted);
                this.purgeComponents();
            }
        }

        public purgeComponents(){
        }
    }
}