module ecs {
    export interface Plugin{
        /**
         * 注册您的插件。
         * @param b 
         */
        setup(b: WorldConfigurationBuilder);
    }
}