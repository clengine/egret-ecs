module ecs {
    /**
     * 具有优先注册权
     */
    export class ConfigurationElement<T> {
        public itemType: any;
        public item: T;
        public priority: number;

        constructor(item: T, priority: number){
            this.item = item;
            this.itemType = egret.getQualifiedSuperclassName(item);
            this.priority = priority;
        }

        /**
         * 创建Registerable的实例
         * @param item 
         * @param priority 
         */
        public static of<T>(item: T, priority: number = Priority.NORMAL): ConfigurationElement<T>{
            return new ConfigurationElement<T>(item, priority);
        }
    }
}