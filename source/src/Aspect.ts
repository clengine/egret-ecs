module ecs {
    export class Aspect{
       
    }

    export class Builder {
        /**
         * 
         * @param world 
         */
        public build(world: World){
            let tf = world.getComponentManager().typeFactory;
            let aspect = new Aspect();
            
            return aspect;
        }

        private static associate(tf: ComponentTypeFactory, types: Bag<ComponentTypeFactory>, componentBits: BitVector){
            for (let i in types){
                componentBits.set(tf.getIndexFor(types[i]));
            }
        }
    }
}