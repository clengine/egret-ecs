module ecs{
    /** 回收实例的组件类型。 */
    export abstract class PooledComponent extends Component {
        /**
         * 每当组件被回收时调用。实现应该将组件重置为原始状态
         */
        public abstract reset();
    }
}