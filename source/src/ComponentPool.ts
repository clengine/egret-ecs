module ecs{
    export class ComponentPool<T extends PooledComponent>{
        private _type: T;
        private _cache: Bag<T>;

        constructor(type: T){
            this._type = type;
            this._cache = new Bag<T>();
        }
    }
}