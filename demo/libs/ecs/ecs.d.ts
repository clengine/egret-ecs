declare module ecs {
    class Aspect {
    }
    class Builder {
        build(world: World): Aspect;
        private static associate;
    }
}
declare module ecs {
    abstract class BaseSystem {
        protected _world: World;
        constructor();
        setWorld(world: World): void;
        protected begin(): void;
        process(): void;
        protected abstract processSystem(): any;
        protected end(): void;
        protected checkProcessing(): boolean;
        initialize(): void;
        dispose(): void;
    }
}
declare module ecs {
    class AspectSubscriptionManager extends BaseSystem {
        private _subscriptionMap;
        private _subscriptions;
        private _changed;
        private _deleted;
        constructor();
        protected processSystem(): void;
        processNotify(changedBits: BitVector, deletedBits: BitVector): void;
        private toEntityIntBags;
        get(builder: Builder): EntitySubscription;
        private createSubscription;
    }
}
declare module ecs {
    abstract class BaseComponentMapper<A extends Component> {
        type: ComponentType;
        constructor(type: ComponentType);
        abstract create(entityId: number): A;
        abstract remove(entityId: number): any;
        set(entityId: number, value: boolean): A;
    }
}
declare module ecs {
    abstract class BaseEntitySystem extends BaseSystem {
    }
}
declare module ecs {
    class BatchChangeProcessor {
        private _world;
        private _asm;
        changed: BitVector;
        private _deleted;
        constructor(world: World);
        update(): void;
        purgeComponents(): void;
    }
}
declare module ecs {
    abstract class Component {
    }
}
declare module ecs {
    class ComponentManager extends BaseSystem {
        entityToIdentity: NumberBag;
        typeFactory: ComponentTypeFactory;
        private _mappers;
        constructor(entityContainerSize: number);
        protected processSystem(): void;
        getIdentity(entityId: number): number;
        getMapper<T extends Component>(mapper: T): ComponentMapper<T>;
        registerComponentType(ct: ComponentType, capacity: number): void;
        synchronize(es: EntitySubscription): void;
        ensureCapacity(newSize: number): void;
    }
}
declare module ecs {
    class ComponentMapper<A extends Component> extends BaseComponentMapper<A> {
        components: Bag<A>;
        private _pool;
        private _purgatory;
        constructor(type: A, world: World);
        get(entityId: number): A;
        create(entityId: number): A;
        remove(entityId: number): void;
    }
}
declare module ecs {
    class ComponentPool<T extends PooledComponent> {
        private _type;
        private _cache;
        constructor(type: T);
    }
}
declare module ecs {
    abstract class ComponentRemover<A extends Component> {
        pool: ComponentPool<PooledComponent>;
        private _components;
        constructor(components: Bag<A>, pool: ComponentPool<PooledComponent>);
        abstract mark(entity: number): any;
        abstract unmark(entity: number): boolean;
        abstract purge(): any;
        abstract has(entityId: number): boolean;
    }
}
declare module ecs {
    class ComponentType {
        private _type;
        private _index;
        constructor(type: Component, index: number);
        getIndex(): number;
        getType(): Component;
    }
}
declare module ecs {
    class ComponentTypeFactory {
        private _componentTypes;
        types: Bag<ComponentType>;
        initialMapperCapacity: any;
        private _cm;
        constructor(cm: ComponentManager, entityContainerSize: number);
        getIndexFor(c: Component): number;
        getTypeFor(c: Component): ComponentType;
        private createComponentType;
    }
}
declare module ecs {
    class ConfigurationElement<T> {
        itemType: any;
        item: T;
        priority: number;
        constructor(item: T, priority: number);
        static of<T>(item: T, priority?: number): ConfigurationElement<T>;
    }
}
declare module ecs {
    class Entity {
        id: number;
        private _world;
        constructor(world: World, id: number);
        getId(): number;
        getWorld(): World;
    }
}
declare module ecs {
    class EntityManager extends BaseSystem {
        private _entities;
        private _recycled;
        private _entityBitVectors;
        private _limbo;
        private nextId;
        constructor(initialContainerSize: number);
        protected processSystem(): void;
        registerEntityStore(bv: BitVector): void;
        isActive(entityId: number): boolean;
        protected getEntity(entityId: number): Entity;
        createEntityInstance(): Entity;
        private obtain;
        private createEntity;
        private growEntityStores;
    }
}
declare module ecs {
    class EntitySubscription {
        extra: SubscriptionExtra;
        private _activeEntityIds;
        private _entities;
        constructor(world: World, builder: Builder);
        getEntities(): NumberBag;
        rebuildCompressedActives(): void;
    }
    interface SubscriptionListener {
        inserted(entities: NumberBag): any;
        removed(entities: NumberBag): any;
    }
    class SubscriptionExtra {
        aspect: Aspect;
        aspectReflection: Builder;
        constructor(aspect: Aspect, aspectReflection: Builder);
    }
}
declare module ecs {
    class FluidEntityPlugin implements Plugin {
        setup(b: WorldConfigurationBuilder): void;
    }
    class ESubscriptionAspectResolver implements FieldResolver {
        private _aspectFieldResolver;
        initialize(world: World): void;
        resolve(target: Object, fieldType: any, field: Field): any;
    }
}
declare module ecs {
    abstract class SystemInvocationStrategy {
        world: World;
        systems: Bag<BaseSystem>;
        disabled: BitVector;
        abstract process(): any;
        initialize(): void;
        setWorld(world: World): void;
        setSystems(systems: Bag<BaseSystem>): void;
        updateEntityStates(): void;
    }
}
declare module ecs {
    class InvocationStrategy extends SystemInvocationStrategy {
        process(): void;
    }
}
declare module ecs {
    abstract class Manager extends BaseSystem {
        registerManager(): void;
    }
}
declare module ecs {
    interface Plugin {
        setup(b: WorldConfigurationBuilder): any;
    }
}
declare module ecs {
    abstract class PooledComponent extends Component {
        abstract reset(): any;
    }
}
declare module ecs {
    class SuperMapper {
    }
}
declare module ecs {
    class World {
        private _em;
        private _alwaysDelayComponentRemoval;
        private _cm;
        delta: number;
        invocationStrategy: SystemInvocationStrategy;
        batchProcessor: BatchChangeProcessor;
        asm: AspectSubscriptionManager;
        partition: WorldSegment;
        systemsBag: Bag<BaseSystem>;
        constructor(configuration: WorldConfiguration);
        getAspectSubscriptionManager(): AspectSubscriptionManager;
        getComponentManager(): ComponentManager;
        getEntityManager(): EntityManager;
        getMapper<T>(type: T): ComponentMapper<T>;
        isAlwaysDelayComponentRemoval(): boolean;
        setDelta(delta: number): void;
        process(): void;
        compositionId(entityId: number): number;
        setInvocationStrategy(invocationStrategy: SystemInvocationStrategy): void;
        dispose(): void;
        createEntity(): Entity;
    }
    class WorldSegment {
        systems: Map<any, BaseSystem>;
        injector: Injector;
        constructor(configuration: WorldConfiguration);
    }
}
declare module ecs {
    class WorldConfiguration {
        static COMPONENT_MANAGER_IDX: number;
        static ENTITY_MANAGER_IDX: number;
        static ASPECT_SUBSCRIPTION_MANAGER_IDX: number;
        systems: Bag<BaseSystem>;
        private _expectedEntityCount;
        injectables: {
            [s: string]: Object;
        };
        invocationStrategy: SystemInvocationStrategy;
        injector: Injector;
        private _alwaysDelayComponentRemoval;
        private _registered;
        constructor();
        expectedEntityCount(): number;
        setSystem<T extends BaseSystem>(system: T): this;
        setInjector(injector: Injector): WorldConfiguration;
        setInvocationStrategy(invocationStrategy: SystemInvocationStrategy): this;
        isAlwaysDelayComponentRemoval(): boolean;
        setAlwaysDelayComponentRemoval(value: boolean): void;
        initialize(world: World, injector: Injector, asm: AspectSubscriptionManager): void;
        private initializeSystems;
    }
}
declare module ecs {
    class WorldConfigurationBuilder {
        private _systems;
        private _fieldResolvers;
        private _alwaysDelayComponentRemoval;
        private _cache;
        private invocationStrategy;
        constructor();
        build(): WorldConfiguration;
        private registerInvocationStrategies;
        private registerSystem;
        registerFieldResolvers(config: WorldConfiguration): void;
        dependsOn(priority?: number, ...types: any[]): WorldConfigurationBuilder;
        with(priority?: number, ...systems: BaseSystem[]): this;
        register(...fieldResolvers: FieldResolver[]): this;
        protected dependsOnSystem(priority: number, type: BaseSystem): void;
        private addSystems;
        private containsType;
        private reset;
        alwaysDelayComponentRemoval(value: boolean): WorldConfigurationBuilder;
    }
    abstract class Priority {
        static LOWEST: number;
        static LOW: number;
        static OPERATIONS: number;
        static NORMAL: number;
        static HIGH: number;
        static HIGHEST: number;
    }
}
declare module ecs {
    class AspectFieldResolver implements FieldResolver {
        initialize(world: World): void;
        resolve(target: Object, fieldType: any, field: Field): void;
    }
}
declare module ecs {
    class CachedClass {
        clazz: any;
        constructor(clazz: any);
        allFields: Field[];
        injectInherited: boolean;
        failOnNull: boolean;
    }
}
declare module ecs {
    class CachedInjector implements Injector {
        private _cache;
        private _fieldHandler;
        private _injectables;
        getRegistered<T>(id: string): T;
        initialize(world: World, injectables: {
            [s: string]: Object;
        }): void;
        inject(target: Object): void;
        private injectField;
        private getAllInjectableFields;
        private collectDeclaredInjectableFields;
        isInjectable(target: Object): boolean;
        setFieldHandler(fieldHandler: FieldHandler): Injector;
    }
}
declare module ecs {
    enum ClassType {
        MAPPER = 0,
        SYSTEM = 1,
        WORLD = 2,
        CUSTOM = 3
    }
}
declare module ecs {
    class FieldHandler {
        private _cache;
        fieldResolvers: Bag<FieldResolver>;
        constructor(cache: InjectionCache, fieldResolvers?: Bag<FieldResolver>);
        initialize(world: World, injectables: {
            [s: string]: Object;
        }): void;
        addFieldResolver(fieldResolver: FieldResolver): void;
    }
}
declare module ecs {
    interface FieldResolver {
        initialize(world: World): any;
        resolve(target: Object, fieldType: any, field: Field): any;
    }
}
declare module ecs {
    class InjectionCache {
        private _fieldClassTypeCache;
        private _classCache;
        private static _instance;
        static get(): InjectionCache;
        getFieldClassType(fieldType: any): ClassType;
        getCachedClass(clazz: any): CachedClass;
    }
}
declare module ecs {
    interface Injector {
        getRegistered<T>(id: string): T;
        initialize(world: World, injectables: {
            [s: string]: Object;
        }): any;
        inject(target: Object): any;
        isInjectable(target: Object): boolean;
        setFieldHandler(fieldHandler: FieldHandler): Injector;
    }
}
declare module ecs {
    interface PojoFieldResolver extends FieldHandler {
        setPojos(pojos: {
            [s: string]: Object;
        }): any;
    }
}
declare module ecs {
    interface UseInjectionCache {
        setCache(cache: InjectionCache): any;
    }
}
declare module ecs {
    class EntityLinkManager extends BaseEntitySystem {
        linkSites: Bag<LinkSite>;
        decoratedLinkSite: Bag<LinkSite>;
        private _requireListener;
        private _fireEventsOnRegistration;
        constructor(processSitesEvenIfNoListener?: boolean, fireEventsOnRegistration?: boolean);
        protected processSystem(): void;
        private processLink;
    }
}
declare module ecs {
    abstract class LinkSite implements SubscriptionListener {
        subscription: EntitySubscription;
        field: Field;
        type: ComponentType;
        mapper: ComponentMapper<Component>;
        activeEntityIds: BitVector;
        constructor(world: World, type: ComponentType, field: Field);
        inserted(entities: NumberBag): void;
        protected abstract insert(id: number): any;
        removed(entities: NumberBag): void;
        protected abstract remove(id: number): any;
        process(): void;
        protected abstract check(id: number): any;
    }
}
declare module ecs {
    class ClassReflection {
        static isAssignableFrom(c1: any, c2: any): boolean;
    }
}
declare module ecs {
    class Field {
    }
}
declare module ecs {
    class Bag<E> implements ImmutableBag<E> {
        private _data;
        protected _size: number;
        constructor(capacity?: number);
        get(index: number): E;
        getData(): E[];
        set(index: number, e: E): void;
        ensureCapacity(index: number): void;
        unsafeSet(index: number, e: E): void;
        getCapacity(): number;
        size(): number;
        isEmpty(): boolean;
        add(e: E): void;
        contains(e: E): boolean;
        private grow;
    }
}
declare module ecs {
    class BitVector {
        private _words;
        constructor(nbits?: number);
        get(index: number): boolean;
        set(index: number, value?: boolean): void;
        unsafeGet(index: number): boolean;
        unsafeSet(index: number, value?: boolean): void;
        unsafeClear(index: number): void;
        clear(index?: number): void;
        length(): number;
        isEmpty(): boolean;
        toIntBag(out: NumberBag): NumberBag;
        toIntBagIdCid(cm: ComponentManager, out: NumberBag): NumberBag;
        private prepareBag;
        cardinality(): number;
        private bitCount;
        private numberOfLeadingZeros;
        ensureCapacity(bits: number): void;
        private checkCapacity;
    }
}
declare interface Number {
    rightMove(pos: number): number;
}
declare interface Array<T> {
    copy(srcPos: number, dest: Array<T>, destPos: number, length: number): any;
}
declare module ecs {
    interface ImmutableBag<E> {
        get(index: number): E;
        size(): number;
        isEmpty(): boolean;
        contains(e: E): boolean;
    }
}
declare module ecs {
    class Map<T, U> {
        private _keys;
        private _values;
        constructor(init: {
            key: T;
            value: U;
        }[]);
        get(key: T): U;
        add(key: T, value: U): void;
        remove(key: T): void;
        keys(): T[];
        values(): U[];
        clear(): void;
    }
}
declare module ecs {
    class MathUtils {
        static clamp(value: number, min: number, max: number): number;
    }
}
declare module ecs {
    class NumberBag implements ImmutableBag<number> {
        private _data;
        private _size;
        constructor(capacity?: number);
        get(index: number): number;
        size(): number;
        setSize(size: number): void;
        getData(): number[];
        isEmpty(): boolean;
        ensureCapacity(index: number): void;
        private grow;
        contains(value: number): boolean;
    }
}
declare module ecs {
    class NumberDeque {
        private _elements;
        private _beginIndex;
        private _size;
        constructor(capacity?: number);
        contains(e: number): boolean;
        get(index: number): number;
        size(): number;
        getCapacity(): number;
        isEmpty(): boolean;
        add(e: number): void;
        set(index: number, e: number): void;
        ensureCapacity(index: number): void;
        popFirst(): number;
        private assertNotEmpty;
        private grow;
        private index;
    }
}
