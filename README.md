# egret-ecs
基于Egret开发的ECS框架，使之更加易用.

> 该项目还在施工中!

## 项目计划
- 高性能
- 可选的对象池组件，通过字节码编来利用。
- 序列化，json或二进制
- 组件和系统的便捷依赖关系矩阵
- Fluid实体，方便实体访问和组装
- 实体链接管理器用以清理无效的实体引用

### 对象池
自动重写组件，以便回收实例。此功能有助于缓解与垃圾收集相关的冻结和在Android平台上的卡顿现象。

### 组件依赖矩阵

用以生成html报告，描述系统，管理器和组件之间的关系

### Fluid 实体设计
Fluid Entity API提供了一种方便的方式来组装和与您的实体进行交互，从而使代码不再那么冗长，从而提高了可读性。

```typescript
 E(entityId)
      .tag("boss")
      .groups("enemy", "treasure")
      .pos(10,10)
      .animSprite("junkdog")
      .animLayer(1000)
      .invisible(true);
```

### 实体链接管理器 设计
当组件引用一个实体并且该实体被删除时会发生什么？这个可选模块将为您管理实体引用，用以轻松处理复杂的关系。

```typescript
class Anchor extends Component {
    public target: number = -1;
    public target2: Entity;
}
```

#### 用途

- 防止直接引用实体
- 目标实体死亡时删除引用组件
- 父节点删除时删除子实体

#### 局限性

- 要求字段为 `public`